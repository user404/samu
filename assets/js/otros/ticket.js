//alert(dni);
$("#dni").focusout(buscarPostulante);

function buscarPostulante() {
	var dni = $("#dni").val();
	$.ajax({  
        async:true,
        cache:false,
        url: "/persona_ajax/" + dni,  
        success: function(data){      

        if(data.longitud>=1){

           $("#verificacion").fadeIn(1000, function(){
             $("#verificacion").addClass('alert alert-info');
             $("#verificacion").css('text-align','center');
             $("#verificacion").html('<p><strong>El postulante ya existe,</strong> sera redireccionado</p>'); 
           });
           $("#verificacion").fadeOut(6000, function(){
             $("#verificacion").removeClass('alert alert-info');
             $("#verificacion").html(''); 
             document.location.href='http://localhost:1337/persona/show/' + data.id; 
            });
            
         } else {

          $("#verificacion").fadeIn(1000, function(){
             $("#verificacion").addClass('alert alert-success');
             $("#verificacion").css('text-align','center');
             $("#verificacion").html('<p><strong>El postulante es nuevo,</strong> complete todos los campos</p>'); 
           });
           $("#verificacion").fadeOut(10000, function(){
             $("#verificacion").removeClass('alert alert-success');
             $("#verificacion").html('');
            });
            

         }
          /*
          if(data.length>=1)
          {
            console.log('El documento es valido');
         //   $('#verificacion').html( '<span class="glyphicon glyphicon-ok" style="color:green"></span>');
             document.location.href= "/persona/edit/" + dni ; //dni + '/edit';               
          }
          else
          {
            console.log('El documento no es valido');
           // $('#verificacion').html( '<span class="glyphicon glyphicon-remove" style="color:red"></span>');
          }
          */
          
        } 
    });
}



/*
function buscarPostulante1() {
	var dni = $("#dni").val();
	$.ajax({  
        async:true,
        cache:false,
        url: ,  
        success: function(data){          
          if(data.valor == "true")
          {
            console.log('El documento es valido');
            $('#verificacion').html( '<span class="glyphicon glyphicon-ok" style="color:green"></span>');
             document.location.href=dni + '/edit';               
          }
          else
          {
            console.log('El documento no es valido');
            $('#verificacion').html( '<span class="glyphicon glyphicon-remove" style="color:red"></span>');
          }
          
        } 
    });
}
*/