/**
 * TicketController
 *
 * @description :: Server-side logic for managing tickets
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    atender:function(req, res){
        
       var atendido;
       if(req.param('atendido') == 0){ atendido = 1 } else {atendido = 0}
        
        var objeto = { 
             atendido: atendido
        }

        Ticket.update(req.param('id'), objeto, function ticketUpdated(err, ticket) {
            if (err) {
                req.session.flash = { err: err }
                return res.redirect('ticket/edit/' + req.param('id'));
            }
            res.redirect('/ticket/show/' + req.param('id'));  
        });
    },
	
    new:function(req, res){
    		Tema.find(function temaFounded(err, objetos) {
                res.view({
                    temas: objetos
                });
            });
    	},

	store:function(req, res){
		var objeto={
            tema : req.param('tema'),
            asunto : req.param('asunto'),
            turno : req.param('turno'),
            estado : req.param('estado'),
            atendido : 0, 
            usuario : req.session.User.id,
        }

        var persona={
            nombre : req.param('nombre'),
            apellido : req.param('apellido'),
            telefono : req.param('telefono'),
            dni : req.param('dni'),
            email : req.param('email')
        }

        Persona.findOne().where({'dni': persona.dni}).exec(function callBack(err,results){
           if(results){    
                objeto.persona = results.id;
               // Ticket.create(objeto, function(err, objetoTicket) {
                var error = 'El usuario debe ser unico';
                    req.session.flash= {
                    err:error
                }
                    res.redirect('ticket/new/');
              //  });
            } else{
                Persona.create(persona, function(err, objetoPersona) {
                    objeto.persona = objetoPersona.id;
                    Ticket.create(objeto, function(err, objetoTicket) {

                      var transicion = {
                        usuario : req.session.User.id,
                        ticket : objetoTicket.id,
                        transicion : 1,
                        area : 1                        
                      }

                      TransicionTicket.create(transicion, function(err, objetoTransicion){
                        res.redirect('ticket/show/' + objetoTicket.id);
                      });

                      
                    });
                });                
            }
        });
    },

    buscar: function(req, res){
        Persona.findOne().where({'dni': persona.dni}).exec(function callBack(err,results){
           if(results){    
                objeto.persona = results.id;
                Ticket.create(objeto, function(err, objetoTicket) {
                    res.redirect('ticket/show/' + objetoTicket.id);
                });
            } else{
                Persona.create(persona, function(err, objetoPersona) {
                    objeto.persona = objetoPersona.id;
                    Ticket.create(objeto, function(err, objeto) {
                        res.redirect('ticket/show/' + objeto.id);
                    });
                });                
            }
        });
    },

    show: function(req, res, next) {
        Ticket.findOne(req.param('id')).populate('respuestas', 
          { sort: 'createdAt DESC' }).populate('usuario').populate('tema').then(function(post) {
//          if(post.tema == null){}
        var commentUsers = Usuario.find({
        id: _.pluck(post.respuestas, 'usuario')
          //_.pluck: Retrieves the value of a 'user' property from all elements in the post.comments collection.
      })
      .then(function(commentUsers) {
        return commentUsers;
      });
    return [post, commentUsers];
  })
  .spread(function(post, commentUsers) {
    commentUsers = _.indexBy(commentUsers, 'id');
    //_.indexBy: Creates an object composed of keys generated from the results of running each element of the collection through the given callback. The corresponding value of each key is the last element responsible for generating the key
    post.respuestas = _.map(post.respuestas, function(respuesta) {
      respuesta.usuario = commentUsers[respuesta.usuario];
      return respuesta;
    });
      
      var date = post.createdAt;      
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  post.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
            
            post.respuestas.forEach(function(respuesta){
                  var date = respuesta.createdAt;
                  
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  respuesta.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
                }); 

      res.view({
                ticket: post
            });

  })
  .catch(function(err) {
    return res.serverError(err);
  });
    },

    edit: function(req, res, next) {
          Ticket.findOne(req.param('id')).populate('persona').exec(function ticketFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();

            Tema.find(function temaEncontrado(err, objetos) 
            {           
                res.view({
                    temas: objetos,
                    ticket: objeto
                });
            });
        });
    },

    update: function(req, res, next) {

        var objeto={
            tema : req.param('tema'),
            asunto : req.param('asunto'),
			turno : req.param('turno'),
			estado : req.param('estado'),
			atendido : req.param('atendido'),
            visitas : req.param('visitas')
        }

        Ticket.update(req.param('id'), objeto, function ticketUpdated(err, ticket) {
            if (err) {
                req.session.flash = { err: err }
                return res.redirect('ticket/edit/' + req.param('id'));
            }

            var persona={
                nombre : req.param('nombre'),
                apellido : req.param('apellido'),
                telefono : req.param('telefono'),
                dni : req.param('dni'),
                email : req.param('email')
            }
            
            Persona.update(ticket.persona, persona, function personaUpdated(err, objeto) {
            if (err) {
                req.session.flash = {
                    err: err
                }
                return res.redirect('ticket/edit/' + req.param('id'));
            }

            res.redirect('/ticket/show/' + req.param('id'));
        });

            
        });
    },

    index: function(req, res, next) {

     //    if (!req.isSocket) {return res.badRequest();}
    // Have the socket which made the request join the "funSockets" room
   // sails.sockets.join('funSockets');
    // Broadcast a "hello" message to all the fun sockets.
    // This message will be sent to all sockets in the "funSockets" room,
    // but will be ignored by any client sockets that are not listening-- i.e. that didn't call `io.socket.on('hello', ...)`
   // sails.sockets.broadcast('hello', {id: '1'});
   




/*
   sails.sockets.blast('hello', {id: '1'});
    // Respond to the request with an a-ok message
    return res.ok();
*/

//ULTIMO
/*
        if(req.session.User.admin == 1) {
            Ticket.find().populate('respuestas').populate('tema').exec(function ticketFounded(err, objetos) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                // res.json(objetos);
                res.view({
                    tickets: objetos
                });
            });
        }
        else {

            Ticket.find().populate('respuestas').populate('tema').exec(function ticketFounded(err, objetos) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                // res.json(objetos);
                res.view({
                    tickets: objetos
                });
            });

        }
*/
      var Promise = require('bluebird');
//substring(MAX(createdAt), 1,4 )
// + TIME(MAX(createdAt))
          var userQueryAsync = Promise.promisify(Ticket.query);
          userQueryAsync("SELECT t.id, t.atendido, t.asunto, r.asunto respuestas_asunto, t.visitas, p.dni, p.apellido, p.nombre, tt.transicion, tt.area"
            + " FROM ticket t"
            + " left join (select ticket, MAX(asunto) asunto,"             
            + " CONCAT(substring(MAX(createdAt), 6,2 ), '/',  substring(MAX(createdAt), 9,2 ), '/', substring(MAX(createdAt), 1,4 ), ' ', TIME(MAX(createdAt)) )  fecha from respuesta group by ticket) r on t.id = r.ticket"
            + " left join Tema ON t.tema = tema.id"
            + " left join Persona p ON t.persona = p.id"
            + " left join (select MAX(tt.usuario) usuario, MAX(tt.ticket) ticket, MAX(tt.transicion) transicion, MAX(tt.area) area, tt.id, MAX(createdAt) fecha_sin_formato FROM TransicionTicket tt group by tt.id) tt ON t.id = tt.ticket"            
            + " left join Area a ON tt.area = a.id"
            + " left join Usuario u ON a.id = u.area"
            + " where (tt.transicion = 1 AND tt.area = 1 AND " + req.session.User.area  + " = tt.area AND DATE_FORMAT(tt.fecha_sin_formato, '%Y-%m-%d') = CURDATE())"
            + " OR (tt.transicion <> 1 AND tt.area <> 1 AND " + req.session.User.area  + " = tt.area AND  DATE_FORMAT(tt.fecha_sin_formato, '%Y-%m-%d') = CURDATE())"
            + " OR (" + req.session.User.admin + " = 1)"
            + " order by t.atendido desc"
//EN LA VISTA PRINCIPAL SI ES DEL AREA 1 SOLO PUEDE VER LOS DEL DIA Y LOS NO DERIVADOS AL PASAR LAS 24 HORAS EL TICKET CAMBIA A UN ESTADO ESPECIAL

//EN LA VISTA PRINCIPAL DE LOS DEMAS AREAS 
            )
          .then(function(user) {

              Transicion.find(function transicionFounded(err, transiciones) {

                  Area.find(function areaFounded(err, objetos) {
                      if (err) {
                          console.log(err);
                          return next(err);
                      }
                          res.view({
                              areas: objetos,
                              transiciones: transiciones,
                              tickets: user
                       //   });
                        });
                  });
              });





          });

           /* Transicion.find().exec(function areaFounded(err, transiciones) {
            if (err) 
                console.log(err);
                return next(err);
            
            Area.find().exec(function areaFounded(err, areas) {
              if (err) 
                console.log(err);
                return next(err);

                res.view({
                        transiciones: transiciones,
                        areas: areas,
                        tickets: user
                    });
              });

            
          });
        }*/
    },

    index1: function(req, res, next) {
        /*
       Ticket.find().where({'respuesta.id': 1}).populate('respuestas').populate('tema').populate('persona').exec(function ticketFounded(err, objetos) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                objetos.forEach(function(ticket){
                  var date = ticket.createdAt;
                  
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  ticket.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
                }); 

                res.json(objetos);
            });
        */
/*
                    Ticket.find()
            .populate('respuestas', { limit: 1, sort: 'createdAt DESC' })
            .populate('tema')
            .populate('persona')
            .populate('transicion_ticket', {
             transicion: 2 , ticket: {parent: 'id'}, limit: 1, sort: 'createdAt DESC'

            })
            .exec(function ticketFounded(err, objetos) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                objetos.forEach(function(ticket){
                  var date = ticket.createdAt;
                  
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  ticket.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
                }); 

                res.json(objetos);
            });
            */
            var Promise = require('bluebird');
//substring(MAX(createdAt), 1,4 )
// + TIME(MAX(createdAt))
          var userQueryAsync = Promise.promisify(Ticket.query);
          userQueryAsync("SELECT t.id, t.atendido, t.asunto, r.asunto respuestas_asunto, t.visitas, p.dni, p.apellido, p.nombre, tt.transicion, tt.area"
            + " FROM ticket t"
            + " left join (select ticket, MAX(asunto) asunto,"             
            + " CONCAT(substring(MAX(createdAt), 6,2 ), '/',  substring(MAX(createdAt), 9,2 ), '/', substring(MAX(createdAt), 1,4 ), ' ', TIME(MAX(createdAt)) )  fecha from respuesta group by ticket) r on t.id = r.ticket"
            + " left join Tema ON t.tema = tema.id"
            + " left join Persona p ON t.persona = p.id"
            + " left join (select MAX(tt.usuario) usuario, MAX(tt.ticket) ticket, MAX(tt.transicion) transicion, MAX(tt.area) area, tt.id, MAX(createdAt) fecha_sin_formato FROM TransicionTicket tt group by tt.id) tt ON t.id = tt.ticket"            
            + " left join Area a ON tt.area = a.id"
            + " left join Usuario u ON a.id = u.area"
            + " where (tt.transicion = 1 AND tt.area = 1 AND " + req.session.User.area  + " = tt.area AND DATE_FORMAT(tt.fecha_sin_formato, '%Y-%m-%d') = CURDATE())"
            + " OR (tt.transicion <> 1 AND tt.area <> 1 AND " + req.session.User.area  + " = tt.area AND  DATE_FORMAT(tt.fecha_sin_formato, '%Y-%m-%d') = CURDATE())"
            + " OR (" + req.session.User.admin + " = 1)"
            + " order by t.atendido desc"
//EN LA VISTA PRINCIPAL SI ES DEL AREA 1 SOLO PUEDE VER LOS DEL DIA Y LOS NO DERIVADOS AL PASAR LAS 24 HORAS EL TICKET CAMBIA A UN ESTADO ESPECIAL

//EN LA VISTA PRINCIPAL DE LOS DEMAS AREAS 
            )
          .then(function(user) {
            return res.json(user);
          });
/*
  + " where (tt.transicion = 1 AND tt.area = 1 AND t.usuario = " + req.session.User.id 
            + ") OR (tt.transicion != 1 AND tt.area != 1 AND tt.area = u.area AND t.usuario = " + req.session.User.id +") order by t.atendido desc"
            */

       /*
        if(req.session.User.admin == 1) {
            Ticket.find().populate('respuestas', { limit: 1, sort: 'createdAt DESC' }).populate('tema').populate('persona').exec(function ticketFounded(err, objetos) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                objetos.forEach(function(ticket){
                  var date = ticket.createdAt;
                  
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  ticket.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
                }); 

                res.json(objetos);
            });
        } else {
            Ticket.find().where({'usuario': req.session.User.id})
            .populate('respuestas', { limit: 1, sort: 'createdAt DESC' })
            .populate('tema')
            .populate('persona')
            .populate('transicion_ticket', {
             transicion: 2 ,limit: 1, sort: 'createdAt DESC'

            })
            .exec(function ticketFounded(err, objetos) {
                if (err) {
                    console.log(err);
                    return next(err);
                }
                objetos.forEach(function(ticket){
                  var date = ticket.createdAt;
                  
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  ticket.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
                }); 

                res.json(objetos);
            });
        }
        */
    },



    destroy: function  (req, res, next) {
        Ticket.destroy(req.param('id'), function ticketDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/ticket');
        });
    }

};