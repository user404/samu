/**
 * RespuestaController
 *
 * @description :: Server-side logic for managing respuestas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	new:function(req, res){
		res.view();
	},

store:function(req, res){
        var rta={
            asunto : req.param('tipoAsunto') + req.param('asunto'),
            tipo : req.param('tipo'),
            ticket: parseInt(req.param('ticketId')),
            usuario : req.session.User.id
    }

    Respuesta.create(rta, function(err, respu) {
        Respuesta.findOne(respu.id).populate('ticket').then( function(objeto) {
            
       // EL 0 INDICA QUE NO ES UNA RESPUESTA SINO UNA "SOLUCION" (FIN DEL TRAMITE).
        if (objeto.tipo == 0){

            var ticket={
                estado : 2
            }

                Ticket.update(objeto.ticket.id, ticket, function ticketUpdated(err, ticket) {
                      res.redirect('ticket/show/' + objeto.ticket.id);
                }); 

            } else {
            var visita_ticket = objeto.ticket.visitas + 1;
            var ticket={
                visitas : visita_ticket
            }

            Ticket.update(objeto.ticket.id, ticket, function ticketUpdated(err, ticket) {
                      res.redirect('ticket/show/' + objeto.ticket.id);
            }); 
               
            }
        });
    });
    },

    show: function(req, res, next) {
        Respuesta.findOne(req.param('id'), function respuestaFounded(err, objeto) {
            if (err)
                return next(err);
            res.view({
                respuesta: objeto
            });
        });
    },

    edit: function(req, res, next) {
        Respuesta.findOne(req.param('id'), function respuestaFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();
            res.view({
                respuesta: objeto
            });
        });
    },

    update: function(req, res, next) {
        var objeto={
            asunto : req.param('asunto'),
			tipo : req.param('tipo')
        }

        Respuesta.update(req.param('id'), objeto, function respuestaUpdated(err, objetos) {
            if (err) {
                req.session.flash = {
                    err: err
                }
                return res.redirect('respuesta/edit/' + req.param('id'));
            }            

            res.redirect('/respuesta/show/' + req.param('id'));
        });
    },

    index: function(req, res, next) {
        Respuesta.find(function respuestaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }

            res.view({
                respuestas: objetos
            });
        });
    },

    destroy: function  (req, res, next) {
        Respuesta.destroy(req.param('id'), function respuestaDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/respuesta');
        });
    }

};