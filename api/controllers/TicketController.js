/**
 * TicketController
 *
 * @description :: Server-side logic for managing tickets
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

      newSolo:function(req, res){
        res.view();
      },


    storeTicket: function(req, res){

        var objeto={
            asunto : req.param('asunto'),
//            turno : req.param('turno'),
            estado : 1,
            visitas : 1,
            usuario :  req.session.User.id
        }
        var id_ticket = req.param('id');

        Ticket.update(id_ticket, objeto, function ticketUpdated(err, ticket1) {

          Ticket.findOne(id_ticket).populate('transicionticket', { sort: 'createdAt DESC', limit: 1 }).exec(function ticketFounded(err, ticket) {       

              var persona={
                  nombre : req.param('nombre'),
                  apellido : req.param('apellido'),
                  telefono : req.param('telefono'),
                  email : req.param('email')
              }
              var id_persona = ticket.persona;

              Persona.update(ticket.persona, persona, function personaUpdated(err, objeto) {
              if (err) {
                  req.session.flash = {
                      err: err
                  }
                }

                var transicion = {
                  usuario : req.session.User.id
                }

                TransicionTicket.update(ticket.transicionticket.id, transicion, function ticketUpdated(err, ticket) { 
                  res.redirect('/ticket_listar/3');  
                });
              });
          });
            
        });
    },


    new:function(req, res){
    		Tema.find(function temaFounded(err, objetos) {
            Ticket.find().limit(1).sort('id DESC').exec(function ticketFounded(err, ticket){
              
               if (ticket.length == 0) {
                res.view({
                      temas: objetos,
                      turno: 'AAA1'
                });
              } else {

             var str = ticket[0].turno;
             var hasta = str.length,
                      desde = 3,
                      cortado = str.substr(0, 3);


                      var resul = parseInt(str.substr(desde, hasta));
                      if(resul < 99) {
                        resul += 1;
                        cortado = cortado + resul.toString();
                      } else {    
                        resul = 1;
                        cortado = ((parseInt(cortado, 36)+1).toString(36)).replace(/0/g,'a').toUpperCase() + resul.toString();
                      }

              res.view({
                      temas: objetos,
                      turno: cortado
                  });
            } 
            });     
        });
  	},

  transicion:function(req, res){
        var objeto={
            usuario : req.session.User.id,
            ticket : req.param('idTicket'),
            area : req.param('area'),
            }

        TransicionTicket.create(objeto, function(err, objetos) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }
                return res.redirect('ticket/new');
            }

            Area.findOne(req.param('area'), function areaFounded(err, area) {
              var respuesta={
                asunto : 'Se derivo al area de ' +  area.nombre + ': ' + req.param('asunto'),
                tipo : req.param('tipo'),
                ticket: parseInt(req.param('ticketId')),
                usuario : req.session.User.id
              }

              Respuesta.create(respuesta, function(err, objeto) {
                res.redirect('ticket');
              })

          })

        });
    },

	store:function(req, res){
		var objeto={
            tema : req.param('tema'),
            asunto : req.param('asunto'),
            turno : req.param('turno'),
            estado : req.param('estado'),
            usuario : req.session.User.id,
        }

        var persona={
            nombre : req.param('nombre'),
            apellido : req.param('apellido'),
            telefono : req.param('telefono'),
            dni : req.param('dni'),
            email : req.param('email')
        }

        Persona.findOne().where({'dni': persona.dni}).exec(function callBack(err,results){
           if(results){    
                objeto.persona = results.id;
                var error = 'El usuario debe ser unico';
                    req.session.flash= {
                    err:error
                }
                    res.redirect('ticket/new/');
            } else{
                Persona.create(persona, function(err, objetoPersona) {
                    objeto.persona = objetoPersona.id;
                    Ticket.create(objeto, function(err, objetoTicket) {

                      var transicion = {
                        usuario : req.session.User.id,
                        ticket : objetoTicket.id,
                        area : req.param('area')                      
                      }



                      TransicionTicket.create(transicion, function(err, objetoTransicion){
                      sails.sockets.blast('recargarTabla', {
                          ticket : objetoTicket
                      });

                      var str = objetoTicket.turno;
                      var hasta = str.length,
                      desde = 3,
                      cortado = str.substr(0, 3);


                      var resul = parseInt(str.substr(desde, hasta));
                      if(resul < 99) {
                        resul += 1;
                        cortado = cortado + resul.toString();
                      } else {    
                        resul = 1;
                        cortado = ((parseInt(cortado, 36)+1).toString(36)).replace(/0/g,'a').toUpperCase() + resul.toString();
                      }

                      sails.sockets.blast('incrementarContador', {
                          turno : cortado
                      });
                        if(req.session.User.area.id == req.param('area')  ||  req.session.User.admin == 1){
                          res.redirect('ticket/show/' + objetoTicket.id);
                        } else{
                          res.redirect('ticket');
                        }
                      });

                      
                    });
                });                
            }
        });
    },

    buscar: function(req, res){
        Persona.findOne().where({'dni': persona.dni}).exec(function callBack(err,results){
           if(results){    
                objeto.persona = results.id;
                Ticket.create(objeto, function(err, objetoTicket) {
                    res.redirect('ticket/show/' + objetoTicket.id);
                });
            } else{
                Persona.create(persona, function(err, objetoPersona) {
                    objeto.persona = objetoPersona.id;
                    Ticket.create(objeto, function(err, objeto) {
                        res.redirect('ticket/show/' + objeto.id);
                    });
                });                
            }
        });
    },


    show: function(req, res, next) {
        Ticket.findOne(req.param('id')).populate('respuestas', { sort: 'createdAt DESC' }).populate('usuario').populate('tema').then(function(post) {
        var commentUsers = Usuario.find({
        id: _.pluck(post.respuestas, 'usuario')
      })
      .then(function(commentUsers) {
        return commentUsers;
      });
    return [post, commentUsers];
  })
  .spread(function(post, commentUsers) {
    commentUsers = _.indexBy(commentUsers, 'id');
    post.respuestas = _.map(post.respuestas, function(respuesta) {
      respuesta.usuario = commentUsers[respuesta.usuario];
      return respuesta;
    });
      
      var date = post.createdAt;      
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12;
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  post.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
            
            post.respuestas.forEach(function(respuesta){
                  var date = respuesta.createdAt;
                  
                  var hours = date.getHours();              
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'pm' : 'am';
                  hours = hours % 12;
                  hours = hours ? hours : 12; 
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  respuesta.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
                });  

      TransicionTicket.find().sort('createdAt DESC').where({ ticket : req.param('id') }).populate('usuario').populate('area').then( function temaFounded(transiciones, err) {
            if (err)
                return next(err);

              Area.find(function areaFounded(err, areas) {
                res.view({
                    ticket: post,
                    transiciones : transiciones,
                    areas : areas
                });
              });
      });

  })
  .catch(function(err) {
    return res.serverError(err);
  });
    },

    edit: function(req, res, next) {
          Ticket.findOne(req.param('id')).populate('persona').exec(function ticketFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();

            Tema.find(function temaEncontrado(err, objetos) 
            {           
                res.view({
                    temas: objetos,
                    ticket: objeto
                });
            });
        });
    },

    update: function(req, res, next) {

        var objeto={
            tema : req.param('tema'),
            asunto : req.param('asunto'),
			      turno : req.param('turno'),
			      estado : req.param('estado'),
            visitas : req.param('visitas')
        }

        Ticket.update(req.param('id'), objeto, function ticketUpdated(err, ticket) {
            if (err) {
                req.session.flash = { err: err }
                return res.redirect('ticket/edit/' + req.param('id'));
            }
            res.redirect('/ticket/show/' + req.param('id'));           
        });
    },

    index: function(req, res, next) {

                  Area.find(function areaFounded(err, objetos) {
                      if (err) {
                          console.log(err);
                          return next(err);
                      }
                          res.view({
                              areas: objetos,
                              atendido: '3'
                       //   });
                        });
                  });
    },

    ticket_listar: function(req, res, next) {

                  Area.find(function areaFounded(err, objetos) {
                      if (err) {
                          console.log(err);
                          return next(err);
                      }
                      res.view('ticket/index', {
                        areas: objetos,
                        atendido: req.param('atendido')
                      });
                  });
    },

    /*  Estados
                        0 = Eliminado
                        1 = En proceso
                        2 = Finalizado
                        3 = En reposo
                        4 = Alta totem      
    */

    index1: function(req, res, next) {    
        var Promise = require('bluebird');
        var userQueryAsync = Promise.promisify(Ticket.query);

        userQueryAsync("SELECT t.estado, t.turno, t.id, coalesce(t.asunto, '') asunto, coalesce(r.asunto, '') respuestas_asunto, t.visitas, tema.tema, p.dni, coalesce(p.apellido, '') apellido, coalesce(p.nombre, 'TODAVIA NO CARGADO') as nombre, tt.area, CONCAT(DATE_FORMAT(tt.fecha_sin_formato, '%d-%m-%Y'), ' ', TIME_FORMAT(tt.fecha_sin_formato, '%H:%i'))  as asignado"
          + " FROM ticket t"
          + " left join (select rta.ticket, asunto,"             
          + " CONCAT(substring(updatedAt, 6,2 ), '/', substring(updatedAt, 9,2 ), '/', substring(updatedAt, 1,4 ), ' ', TIME(updatedAt) ) fecha from respuesta inner join (select ticket, max(id) id from respuesta group by ticket) rta on rta.id = respuesta.id) r on t.id = r.ticket"
          + " left join Tema ON t.tema = tema.id"
          + " left join Persona p ON t.persona = p.id"
          + " left join (select t.usuario, t.ticket, t.area, t.id, t.updatedAt as fecha_sin_formato from TransicionTicket t inner join (SELECT ticket, max(id) id FROM TransicionTicket tt  group by ticket ) tt on tt.id = t.id ) tt ON t.id = tt.ticket"            
          + " left join Area a ON tt.area = a.id"
          + " left join Usuario u ON a.id = u.area"
          + " where ( (" + req.session.User.area.id  + " = tt.area and t.estado != 0 )"
          + " OR (" + req.session.User.admin + " = 1 ))"
          + " AND ((" + req.param('atendido') + " = 0 and t.estado in (1, 3, 4)) or (" + req.param('atendido') + " = 1 and t.estado = 2) or (" + req.param('atendido') + " = 3))"
          + " order by tt.fecha_sin_formato desc"
            )
          .then(function(user) {
            return res.json(user);
          });
    },

    baja: function  (req, res, next) {
        var objeto={
            estado : 0
        }

        Ticket.update(req.param('id'), objeto, function ticketUpdated(err, ticket) {
            var rta={
              asunto : 'ELIMINADO, motivo: ' + req.param('asunto'),
              tipo : req.param('tipo'),
              ticket: req.param('id'),
              usuario : req.session.User.id
            }


            Respuesta.create(rta, function(err, respu) {
              res.redirect('/ticket_listar/3');  
            });
        });
    },

    /*  Estados
                        0 = Eliminado
                        1 = En proceso
                        2 = Finalizado
                        3 = En reposo
                        4 = Alta totem      
    */

    reactivar: function  (req, res, next) {
        var usr = 0;
        if(req.session.User){
          usuario : req.session.User.id
        }

        var id = req.param('id');
        var tip = req.param('tipo');

        Ticket.findOne(id).exec(function ticketFounded(err, objeto) {       
          objeto.visitas += 1;
          var ticket_obj = {
            visitas : objeto.visitas,
            estado : 1
          };

          Ticket.update(id, ticket_obj, function ticketUpdated(err, ticket) {
              var rta={
                asunto : 'El responsable del ticket lo reactivo.',
                tipo : tip,
                ticket: id,
                usuario : usr
              }            

              Respuesta.create(rta, function(err, respu) {
                res.redirect('/persona/newSolo');  
              });              
            });
        });   

    },

    destroy: function  (req, res, next) {
        Ticket.destroy(req.param('id'), function ticketDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/ticket');
        });
    }    
};