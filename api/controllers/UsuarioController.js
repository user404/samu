/**
 * UsuarioController
 *
 * @description :: Server-side logic for managing usuarios
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    subscribe: function(req,res){
         Usuario.find(function usuarioEncontrado(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }

        res.send(200);
        });
    },

    new:function(req, res){
        Area.find(function areaEncontrado(err, objetos) 
        {           
            res.view({
                areas: objetos
            });
        });
    },


	store:function(req, res){
		var objeto={
			nombre: req.param('nombre'),
			apellido: req.param('apellido'),
			usuario: req.param('usuario'),
			clave: req.param('clave'),
            claveConfirmacion: req.param('claveConfirmacion'),
			email: req.param('email'),
 			estado: req.param('estado'),
            area: req.param('area'),
            admin: req.param('admin')
		}


        Usuario.create(objeto, function(err, objeto) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }
                return res.redirect('usuario/new');
            }
            res.redirect('usuario/show/' + objeto.id);
        });
    },

    show: function(req, res, next) {
        Usuario.findOne(req.param('id')).populate('area').exec(function usuarioEncontrado(err, objeto) {
            if (err)
                return next(err);
            res.view({
                usuario: objeto
            });
        });
    },

    edit: function(req, res, next) {
        Usuario.findOne(req.param('id'), function usuarioEncontrado(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();
            var usuario = objeto;

            Area.find(function areaEncontrado(err, objetos) 
            {           
                res.view({
                    areas: objetos,
                    usuario: usuario
                });
            });
        });
    },


    update: function(req, res, next) {
        var objeto={
            nombre: req.param('nombre'),
            apellido: req.param('apellido'),
            usuario: req.param('usuario'),
            clave: req.param('clave'),
            claveConfirmacion : req.param('claveConfirmacion'),
            email: req.param('email'),
            estado: req.param('estado'),
            area: req.param('area'),
            admin: req.param('admin')
        }

        Usuario.update(req.param('id'), objeto, function usuarioUpdated(err, objetos) {
              res.redirect('/usuario/show/' + req.param('id'));
        });
    },

    index: function(req, res, next) {
        Usuario.find(function usuarioEncontrado(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }

            res.view({
                usuarios: objetos
            });
        });
    },

    destroy: function  (req, res, next) {
        Usuario.destroy(req.param('id'), function usuarioDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/usuario');
        });
    },

};