/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var bcrypt = require('bcrypt')
module.exports = {

	new:function (req, res){
//		res.view();
        res.view('session/new', {layout: 'layout-login'})

	},

    subscribe: function(req, res) {
        debugger;
      if (!req.isSocket) {
        return res.badRequest();
      }

      var roomName = 'Sistemas';//req.param('roomName');
      sails.sockets.join(req, roomName, function(err) {
        if (err) {
          return res.serverError(err);
        }

        console.log('se suscribio a ' + roomName);
        return res.json({
          message: 'Subscribed to a fun room called '+roomName+'!'
        });
      });
    },

	create:function (req, res, next){
      sails.sockets.join('IPDUV', function(){
        console.log('se registro');
            })

		var username = req.param('usuario');
		var password = req.param('password');
		if(!username || !password)
            {
            	var noUsernameOrPasswordError = [{message: 'Debe Ingresar un usuario y contraseña'}]
                req.session.flash = {
                    err: noUsernameOrPasswordError
                }
                return res.redirect('/session/new');
            }
        Usuario.findOneByUsuario(username).populate('area').exec( function usuarioEncontrado(err, user) {
        	if (err) {
        		req.session.flash= {
        			err:err
        		}
        		return res.redirect('/session/new');
        	}
        	if(!user){
        		var noUserFoundedError = [{message: 'El usuario no se encuentra'}]
        		req.session.flash = {
                    err: noUserFoundedError
                }
                return res.redirect('/session/new');
        	}

        bcrypt.compare(password, user.claveEncriptada, function passwordMatch(err, valid) {
            if (err) {
                console.log(err);
                req.session.flash= {
                    err:err
                }
                return res.redirect('/session/new');
            }
            if(!valid){
                var passwrodDoNotMatchError = [{message : 'Las contraseñas no coinciden'
            }]
            req.session.flash= {
                    err:passwrodDoNotMatchError
                }
                return res.redirect('/session/new');
            }
            req.session.authenticated = true;
            req.session.User = user;
            sails.sockets.blast('usuarioSeConectado', {
                    user : user
           });

            res.redirect('/ticket');

        });
        	
        });
    },

    logout: function(req, res){
        req.session.destroy();
        return res.redirect('/');
    }

	
	
};

