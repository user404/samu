/**
 * PersonaController
 *
 * @description :: Server-side logic for managing personas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  newSolo:function(req, res){
    Ticket.find().limit(1).sort('id DESC').exec(function ticketFounded(err, ticket){
              
               if (ticket.length == 0) {
                res.view({
                      turno: 'AAA1'
                });
              } else {

             var str = ticket[0].turno;
             var hasta = str.length,
                      desde = 3,
                      cortado = str.substr(0, 3);


                      var resul = parseInt(str.substr(desde, hasta));
                      if(resul < 99) {
                        resul += 1;
                        cortado = cortado + resul.toString();
                      } else {    
                        resul = 1;
                        cortado = ((parseInt(cortado, 36)+1).toString(36)).replace(/0/g,'a').toUpperCase() + resul.toString();
                      }
              res.view('persona/newSolo', {turno: cortado, layout: 'layout-totem'})
//              res.view({ turno: cortado});
            } 
            });  
//    res.view();
  },
  
    storeTicket:function(req, res){
    var objeto={
            tema : req.param('tema'),
            asunto : req.param('asunto'),
            turno : req.param('turno'),
            estado : req.param('estado'),
            usuario : req.session.User.id,
            persona : req.param('idPersona')
        }

            Ticket.create(objeto, function(err, objetoTicket) {

              var transicion = {
                usuario : req.session.User.id,
                ticket : objetoTicket.id,
                transicion : 1,
                area : 1                        
              }

              console.log(transicion);


              TransicionTicket.create(transicion, function(err, objetoTransicion){
                res.redirect('persona/show/' + objetoTicket.persona);
              });

              
            });           

    },

  
    probandoPostAjax:function(req, res){    
      var ticket={
             tema : req.param('tema'),
             estado : 4,
             turno : req.param('turno'),
             persona : 0
          }

        var persona={
            dni : req.param('dni')
        }

        Persona.findOne().where({'dni': persona.dni}).exec(function callBack(err,results){
           if(results){
              res.redirect('persona/newSolo/');
            } else{
                  Persona.create(persona, function(err, objetoPersona) {
                    ticket.persona = objetoPersona.id;
                    Ticket.create(ticket, function(err, objetoTicket) {
                      sails.sockets.blast('recargarTabla', {
                          ticket : objetoTicket
                        });

                      if(!req.session.User){
                        var transicion = {
//                      usuario : req.session.User.id,
                        ticket : objetoTicket.id,
                        area : req.param('area')                      
                        }
                      } else {
                        var transicion = {
                        usuario : req.session.User.id,
                        ticket : objetoTicket.id,
                        area : req.param('area')                      
                        }
                      }




                      var str = objetoTicket.turno;
                      var hasta = str.length,
                      desde = 3,
                      cortado = str.substr(0, 3);


                      var resul = parseInt(str.substr(desde, hasta));
                      if(resul < 99) {
                        resul += 1;
                        cortado = cortado + resul.toString();
                      } else {    
                        resul = 1;
                        cortado = ((parseInt(cortado, 36)+1).toString(36)).replace(/0/g,'a').toUpperCase() + resul.toString();
                      }

                      sails.sockets.blast('incrementarContador', {
                          turno : cortado
                      });


                      TransicionTicket.create(transicion, function(err, objetoTransicion){
                      
                        return res.json({ user: objetoPersona.id, turno : objetoTicket.turno });
//                        res.redirect('persona/newSolo/');
                      });

                      
                    });
                });                
            }
        });
    },




    storePersona:function(req, res){    
      var ticket={
             tema : req.param('tema'),
             estado : 4,
             turno : req.param('turno'),
             persona : 0
          }

        var persona={
            dni : req.param('dni')
        }

        Persona.findOne().where({'dni': persona.dni}).exec(function callBack(err,results){
           if(results){
              res.redirect('persona/newSolo/');
            } else{
                  Persona.create(persona, function(err, objetoPersona) {
                    ticket.persona = objetoPersona.id;
                    Ticket.create(ticket, function(err, objetoTicket) {
                      sails.sockets.blast('recargarTabla', {
                          ticket : objetoTicket
                        });

                      if(!req.session.User){
                        var transicion = {
//                      usuario : req.session.User.id,
                        ticket : objetoTicket.id,
                        area : req.param('area')                      
                        }
                      } else {
                        var transicion = {
                        usuario : req.session.User.id,
                        ticket : objetoTicket.id,
                        area : req.param('area')                      
                        }
                      }




                      var str = objetoTicket.turno;
                      var hasta = str.length,
                      desde = 3,
                      cortado = str.substr(0, 3);


                      var resul = parseInt(str.substr(desde, hasta));
                      if(resul < 99) {
                        resul += 1;
                        cortado = cortado + resul.toString();
                      } else {    
                        resul = 1;
                        cortado = ((parseInt(cortado, 36)+1).toString(36)).replace(/0/g,'a').toUpperCase() + resul.toString();
                      }

                      sails.sockets.blast('incrementarContador', {
                          turno : cortado
                      });

                      TransicionTicket.create(transicion, function(err, objetoTransicion){
                      

                        res.redirect('persona/newSolo/');
                      });

                      
                    });
                });                
            }
        });
    },

    buscarPostulante: function(req, res){
        Persona.findOne().where({'dni': req.param('documento')}).exec(function callBack(err,objetos){
            if (err) {
                console.log(err);
                next(err);
            }

            if(objetos == null){
             var mensaje = {
                'longitud' : '0'
              };
              res.json(mensaje);
            }
            
            else {
                objetos.longitud = 1;
                res.json(objetos);
            }
        });

    },

    index1: function(req, res, next) {
        Ticket.find().where({'persona': req.param('id')}).populate('respuestas').populate('tema').populate('persona').sort('createdAt DESC').exec(function ticketFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }
            objetos.forEach(function(ticket){
              var date = ticket.createdAt;
              
              var hours = date.getHours();              
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
              ticket.creacion = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
            });
            
           res.json(objetos);
        });
    },

	new:function(req, res){
		res.view();
	},

	store:function(req, res){
		var objeto={
			nombre : req.param('nombre'),
			apellido : req.param('apellido'),
			telefono : req.param('telefono'),
			dni : req.param('dni'),
			email : req.param('email')
			}

        Persona.create(objeto, function(err, objeto) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }
                return res.redirect('persona/new');
            }
            res.redirect('persona/show/' + objeto.id);
        });
    },

    show: function(req, res, next) {



        Persona.findOne(req.param('id')).populate('tickets').exec(function ticketFounded(err, objeto) {
            if (err)
                return next(err);
            
              var ultimo_ticket = objeto.tickets.length - 1;
              var date = objeto.tickets[ultimo_ticket].createdAt
              var hours = date.getHours();
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12;
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;

            objeto.ultimo_ticket = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + "  " + strTime;
            
            Tema.find(function temaEncontrado(err, objetos) 
            { 
                Ticket.find().limit(1).sort('id DESC').exec(function ticketFounded(err, ticket){
                  
                   if (ticket.length == 0) {
                    res.view({
                          temas: objetos,
                          turno: 'AAA1'
                    });
                  } else {

                 var str = ticket[0].turno;
                 var hasta = str.length,
                          desde = 3,
                          cortado = str.substr(0, 3);


                          var resul = parseInt(str.substr(desde, hasta));
                          if(resul < 99) {
                            resul += 1;
                            cortado = cortado + resul.toString();
                          } else {    
                            resul = 1;
                            cortado = ((parseInt(cortado, 36)+1).toString(36)).replace(/0/g,'a').toUpperCase() + resul.toString();
                          }

                  res.view({temas: objetos,
                          persona: objeto,
                          turno: cortado
                      });
                } 
                }); 
            });

        });

    },

    edit: function(req, res, next) {
        Persona.findOne(req.param('id'), function personaFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();
            res.view({
                persona: objeto
            });
        });
    },

    update: function(req, res, next) {
        var objeto={
			nombre : req.param('nombre'),
			apellido : req.param('apellido'),
			telefono : req.param('telefono'),
			dni : req.param('dni'),
			email : req.param('email')
        }

        Persona.update(req.param('id'), objeto, function personaUpdated(err, objetos) {
            if (err) {
                req.session.flash = {
                    err: err
                }
                return res.redirect('persona/edit/' + req.param('id'));
            }

            res.redirect('/persona/show/' + req.param('id'));
        });
    },

    index: function(req, res, next) {
        Persona.find(function personaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.view({
                personas: objetos
            });
        });
    },

    destroy: function  (req, res, next) {
        Persona.destroy(req.param('id'), function personaDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/persona');
        });
    },

    tickets_persona: function(req, res, next) {    
        var Promise = require('bluebird');
        var userQueryAsync = Promise.promisify(Ticket.query);
        userQueryAsync("SELECT t.estado, t.id, t.asunto, coalesce(r.asunto, '') respuestas_asunto, t.visitas, tema.tema, p.dni, p.apellido, p.nombre, tt.area, CONCAT(DATE_FORMAT(tt.fecha_sin_formato, '%d-%m-%Y'), ' ', TIME_FORMAT(tt.fecha_sin_formato, '%H:%i'))  as asignado"
          + " FROM ticket t"
          + " left join (select rta.ticket, asunto,"             
          + " CONCAT(substring(createdAt, 6,2 ), '/', substring(createdAt, 9,2 ), '/', substring(createdAt, 1,4 ), ' ', TIME(createdAt) ) fecha from respuesta inner join (select ticket, max(id) id from respuesta group by ticket) rta on rta.id = respuesta.id) r on t.id = r.ticket"
          + " left join Tema ON t.tema = tema.id"
          + " left join Persona p ON t.persona = p.id"
          + " left join (select t.usuario, t.ticket, t.area, t.id, t.createdAt as fecha_sin_formato from TransicionTicket t inner join (SELECT ticket, max(id) id FROM TransicionTicket tt  group by ticket ) tt on tt.id = t.id ) tt ON t.id = tt.ticket"            
          + " left join Area a ON tt.area = a.id"
          + " left join Usuario u ON a.id = u.area"
          + " where (" + req.param('dni') + " = p.dni and t.estado = 3 )"
          + " order by tt.fecha_sin_formato desc"
            )
          .then(function(user) {
            return res.json(user);
          });
    },

    tickets_persona_existe: function(req, res, next) {    
        var Promise = require('bluebird');
        var userQueryAsync = Promise.promisify(Ticket.query);
        console.log();
        userQueryAsync("SELECT p.dni, p.apellido, p.nombre, sum(case when t.estado = 3 then 1 else 0 end) as cantidad"
          + " FROM ticket t left join Persona p ON t.persona = p.id"
          + " where " + req.param('dni') + " = p.dni"
          + " group by p.dni, p.apellido, p.nombre"
            )
          .then(function(user) {
            return res.json(user);
          });
    }



};