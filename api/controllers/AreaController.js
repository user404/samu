/**
 * AreaController
 *
 * @description :: Server-side logic for managing areas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 
module.exports = {

    area_ajax:function(req, res){

          Area.find().sort('nombre ASC').exec(function areaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }

            var areas = [];
            objetos.forEach(function(ar){
                var area = {
                    id : ar.id,
                    text : ar.nombre
                }
                areas.push(area);
            
            });
              return res.json(areas);
        });

    },

	new:function(req, res){
		res.view();
	},

	store:function(req, res){
		var objeto={
			nombre : req.param('nombre'),
 			estado : req.param('estado')
		}


        Area.create(objeto, function(err, objeto) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }
                return res.redirect('area/new');
            }

            Area.message(4, {'nombre': 'hola' });
            res.redirect('area/show/' + objeto.id);
             Area.message(4, {'nombre': 'hola' });
        });
    },

    show: function(req, res, next) {
        Area.findOne(req.param('id'), function areaFounded(err, objeto) {
            if (err)
                return next(err);
            res.view({
                area: objeto
            });
        });
    },

    edit: function(req, res, next) {
        Area.findOne(req.param('id'), function areaFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();
            res.view({
                area: objeto
            });
        });
    },


    update: function(req, res, next) {
        var objeto={
            nombre : req.param('nombre'),
            estado : req.param('estado')
        }

        Area.update(req.param('id'), objeto, function areaUpdated(err, objeto) {
            if (err) {
                req.session.flash = {
                    err: err
                }
                return res.redirect('area/edit/' + req.param('id'));
            }

            res.redirect('/area/show/' + req.param('id'));
        });
    },

    index: function(req, res, next) {
        Area.find(function areaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }

            res.view({
                areas: objetos,
                objeto: 'area'
            });
        });
    },

    index1: function(req, res, next) {
        Area.find(function areaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }
          res.json(objetos);
        });
    },

    destroy: function  (req, res, next) {
        Area.destroy(req.param('id'), function areaDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/area');
        });
    }

};