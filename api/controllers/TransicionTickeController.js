/**
 * TransicionticketController
 *
 * @description :: Server-side logic for managing transicions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	new:function(req, res){
		Transicion.find(function transicionEncontrada(err, objetosArea) 
            {           
                res.view({
                    areas: objetosArea
                });
            });
	},

	store:function(req, res){

		var objeto={
            usuario : req.session.User.id,
            ticket : req.param('idTicket'),
            transicion : req.param('transicion'),
            area : req.param('area'),
            }

        TransicionTicket.create(objeto, function(err, objeto) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }

                sails.sockets.blast('transicion', objeto);
                return res.redirect('transicion/new');
            }
            res.redirect('ticket');
        });
    },
    storeTicket:function(req, res){
        var objeto={
            usuario : req.session.User.id,
            ticket : req.param('idTicket'),
            area : req.param('area'),
            }
            
        TransicionTicket.create(objeto, function(err, objeto) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }
                return res.redirect('transicion/new');
            }
            res.redirect('ticket');
        });
    },

    show: function(req, res, next) {
        Transicion.findOne(req.param('id')).populate('area').exec(function transicionFounded(err, objeto) {
            if (err)
                return next(err);
            res.view({
                transicion: objeto
            });
        });
    },

    edit: function(req, res, next) {
        Transicion.findOne(req.param('id'), function transicionFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();


    Area.find(function areaEncontrado(err, objetosArea) 
            {           
                res.view({
                    areas: objetosArea,
                    transicion: objeto
                });
            });
        });
    },

    update: function(req, res, next) {
        var objeto={
            transicion : req.param('transicion'),
			estado : req.param('estado'),
            area : req.param('area'),
	     }

        Transicion.update(req.param('id'), objeto, function transicionUpdated(err, objetos) {
            if (err) {
                req.session.flash = {
                    err: err
                }
                return res.redirect('transicion/edit/' + req.param('id'));
            }

            res.redirect('/transicion/show/' + req.param('id'));
        });
    },

    index: function(req, res, next) {
        Transicion.find().populate('area').exec(function areaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.view({
                transicions: objetos,
                objeto: 'area'
            });
        });
    },


    destroy: function  (req, res, next) {
        Transicion.destroy(req.param('id'), function transicionDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/transicion');
        });
    }

};