/**
 * TemaController
 *
 * @description :: Server-side logic for managing temas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    tema_ajax:function(req, res){

        Tema.find().where({ area : req.param('idArea') }).sort('tema ASC').exec( function temaFounded(err, objetos) {
            if (err)
                return next(err);
            if (!objetos)
                return next();

              var temas = [];
              objetos.forEach(function(ar){
                var tema = {
                    id : ar.id,
                    text : ar.tema
                }
                temas.push(tema);
            
            });
             return res.json(temas);
        })
    },
	new:function(req, res){
		Area.find(function areaEncontrado(err, objetosArea) 
            {           
                res.view({
                    areas: objetosArea
                });
            });
	},

	store:function(req, res){
		var objeto={
			tema : req.param('tema'),
			estado : req.param('estado'),
            area : req.param('area'),
			}

        Tema.create(objeto, function(err, objeto) {
            if (err) {
                console.log(JSON.stringify(err));
                req.session.flash={
                    err: err
                }
                return res.redirect('tema/new');
            }
            res.redirect('tema/show/' + objeto.id);
        });
    },

    show: function(req, res, next) {
        Tema.findOne(req.param('id')).populate('area').exec(function temaFounded(err, objeto) {
            if (err)
                return next(err);
            res.view({
                tema: objeto
            });
        });
    },

    edit: function(req, res, next) {
        Tema.findOne(req.param('id'), function temaFounded(err, objeto) {
            if (err)
                return next(err);
            if (!objeto)
                return next();


    Area.find(function areaEncontrado(err, objetosArea) 
            {           
                res.view({
                    areas: objetosArea,
                    tema: objeto
                });
            });
        });
    },

    update: function(req, res, next) {
        var objeto={
            tema : req.param('tema'),
			estado : req.param('estado'),
            area : req.param('area'),
	     }

        Tema.update(req.param('id'), objeto, function temaUpdated(err, objetos) {
            if (err) {
                req.session.flash = {
                    err: err
                }
                return res.redirect('tema/edit/' + req.param('id'));
            }

            res.redirect('/tema/show/' + req.param('id'));
        });
    },

    index: function(req, res, next) {
        Tema.find().populate('area').exec(function areaFounded(err, objetos) {
            if (err) {
                console.log(err);
                return next(err);
            }
            res.view({
                temas: objetos,
                objeto: 'area'
            });
        });
    },


    destroy: function  (req, res, next) {
        Tema.destroy(req.param('id'), function temaDestroyed (err) {
            if(err){
                console.log(err);
                return next(err);
            }
            res.redirect('/tema');
        });
    }

};