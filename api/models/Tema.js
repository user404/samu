/**
* Tema.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	tema: {type: 'string'},
  	estado: {type: 'string',
    defaultsTo: '1'},

  	tickets:{
            collection: 'ticket',
            via: 'tema'
        },

    area:{
            model:'area'
        }
          
  	// RELACION CON USUARIO
  	// RELACION CON PERSONA
  	//

  }
};