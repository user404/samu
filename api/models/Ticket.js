/**
* Ticket.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    // id:{
    //   type:'integer',
    //   primaryKey:true
    // },
  	asunto: {
      type: 'string'
    },

/*
  	atendido: {
      type: 'integer'
    },
*/
  	
    visitas: {
      type: 'integer', 
      defaultsTo: '1'
    },
/*
    turno:{
      model:'turno'
    },
*/
    turno: {
      type: 'string',
      defaultsTo: 'AAA0'
    },

    estado: {
      type: 'integer',
      defaultsTo: 1
    },

    persona:{
      model:'persona'
    },

    usuario:{
      model:'usuario'
    },   

    tema:{
      model:'tema'
    },

    respuestas:{
      collection: 'respuesta',
      via: 'ticket'
    },

    transicionticket:{
      collection: 'transicionticket',
      via: 'ticket'
    }

    /*
    ,    
     afterCreate: function(attrs, next) {
      debugger;
      console.log('ENTRO AL AFTER CREATE GATOU');
      console.log(attrs);
      alert('hola');
      next();
    }  	
    */
  }
};