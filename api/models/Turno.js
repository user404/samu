/**
* Turno.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: { 	
  	turno: {type: 'string'},
  	anterior: {type: 'string'},  	
    box: {type: 'string'},
    boxAnterior: {type: 'string'},

    tickets:{
            collection: 'ticket',
            via: 'turno'
        }

  }
};

