/**
* Usuario.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

	  	nombre:{
            type:'string',
            required:true
          },

	  	apellido:{
            type: 'string',
            required:true
            },

	  	usuario:{
            type: 'string',
            //unique:true,
            required:true
            },

      claveEncriptada:{
           type: 'string',
         },

	  	email:{
          type: 'email',
         // unique: true,
          required:true
        },

        admin:{
          type: 'integer',
          required:true,
          defaultsTo : '0'
        },

	  	estado:{
          type: 'integer',
          required:true,
          defaultsTo:'1' 
        },
      online:{
          type:'boolean',
          defaultsTo:false  
      },

	  	area:{
            model:'area',
            required:true
        },

      tickets:{
            collection: 'ticket',
            via: 'usuario'
        },

      respuestas:{
            collection: 'respuesta',
            via: 'ticket'
        },

      transicionticket:{
        collection: 'transicionticket',
        via: 'usuario'
      } 

  },

  beforeCreate: function(values, next) {
    var clave = values.clave;
    var claveConfirmacion = values.claveConfirmacion;
    if (!clave || !claveConfirmacion || clave != values.claveConfirmacion) {
        var claveDoesNotMatchError = [{
            name: 'claveDoesNotMatch',
            message: 'Las Contraseñas deben Coincidir', 
            contra:clave,
            confim:claveConfirmacion
        }]
        return next({
            err: claveDoesNotMatchError
        });
    }

    require('bcrypt').hash(values.clave, 10, function claveEncrypted(err, claveEncriptada) {
        // body...
        values.claveEncriptada = claveEncriptada;
        next();
    });
  },

  
};