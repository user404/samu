/**
* Respuesta.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
 //  	id:{
	//   type:'integer',
	//   primaryKey:true
	// },
  	asunto: {type: 'string'},
  	tipo: {type: 'string', 
      defaultsTo: '1'},

  usuario:{
      model:'usuario'
  },  

  	ticket:{
            model:'ticket'
        }
  }
};

