/**
* Area.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	nombre: {type: 'string'},
  	estado: {type: 'integer', defaultsTo: '0'},

  	usuarios:{
            collection: 'usuario',
            via: 'area'
        },

    temas:{
            collection: 'tema',
            via: 'area'
        },

    toJSON: function(argument) {
      var obj = this.toObject();
      return obj;
    },

    transicionticket:{
      collection: 'transicionticket',
      via: 'area'
    } 
  }
};